from django.http import HttpResponse

from Django_start.db import execute_query


def index(request):
    return HttpResponse("hello")


def get_sales(request):

    result = execute_query("SELECT sum(UnitPrice*Quantity) FROM invoice_items")
    return HttpResponse(str(result[0][0]))

def get_genres(request):

    result = execute_query("""SELECT genres.Name, sum(tracks.Milliseconds * 0.001)
FROM tracks INNER JOIN genres
ON tracks.GenreId = tracks.GenreId 
GROUP by genres.Name""")

    result_list = []
    for item in result:
        result_list.append(str(item))

    response = HttpResponse("\n".join(result_list))
    response.headers['Content-Type'] = 'text/plain'
    return response